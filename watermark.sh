#!/bin/bash
#
# Author: Helmut Flasche <flasche@ient.rwth-aachen.de>, Christian Rohlfing <rohlfing@ient.rwth-aachen.de>

#{{{ Bash settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
#}}}

# Default values
infolder="./pdfs/sorted"
outfolder="./pdfs/watermarked"
tmpfolder=$(mktemp -d)
cores=1
dpi="200"
quality="60"
v=n
author="IENT RWTH Aachen, 2020"

#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [--out outfolder] [--cores numcores] [--quality jpegquality] --  watermark exam scans with matriculation number in folder 'in' and puts them in folder 'out'.
Attention: contents of folder 'out' will be overwritten in the following!

Options:
  -h, --help      show this help text
  -i, --in        input folder with PDFs. Default: ${infolder}
  -o, --out       output folder. Default: ${outfolder}
  -t, --tmp       parent of temporary folder. Default ${tmpfolder}
  -j, --cores     number of cores for parallel processing. Default: ${cores}
  -d, --dpi       dpi parameter for conversion from pdf to images. Default: ${dpi}
  -q, --quality   quality parameter for jpeg. Default: ${quality}
  -a, --author    author of the PDF file. Default: ${author}
  -v, --verbose   "


# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi


OPTIONS=hi:o:j:d:q:vt:a:
LONGOPTS=help,in:,out:,cores:,dpi:,quality:,verbose,tmp:,author:


# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  # e.g. return value is 1
  #  then getopt has complained about wrong arguments to stdout
  exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"


# now enjoy the options in order and nicely split until we see --
while true; do
  case "$1" in
    -h|--help)
      echo "$usage"
      exit
      ;;
    -i|--in)
      infolder="$2"
      shift 2
      ;;
    -t|--tmp)
       tmpfolder="$2"
       shift 2
       ;;
    -o|--out)
      outfolder="$2"
      shift 2
      ;;
    -j|--cores)
      cores="$2"
      shift 2
      ;;
    -d|--dpi)
      dpi="$2"
      shift 2
      ;;
    -q|--quality)
      quality="$2"
      shift 2
      ;;
    -v|--verbose)
      v=y
      shift
      ;;
    -a|--author)
      author="$2"
      shift 2
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "Programming error"
      exit 3
      ;;
  esac
done

# Check folders
for f in \
        "$infolder" \
        "$outfolder" \
        "$tmpfolder"
do
  if ! [ -d "$f" ]; then
    echo "Folder $f does not exist. Exiting."
    exit
  fi
done
#}}}


export SHELL=$(type -p bash)
watermark () {
  longinpdf=$1
  outfolder=$2
  dpi=$3
  quality=$4
  lauthor=$5

  # Get matriculation number from file
  inpdf=$(basename "${longinpdf%.*}")  # file name without folder and extension
  matnum=${inpdf:0:6}  # read in first 6 letters

  case $matnum in
    ''|*[!0-9]*) echo "Warning: ${matnum} not a matriculation number. I'm ignoring it for now." && exit ;;
  esac

  # Watermarking
  echo "Watermarking ${longinpdf} with \"${matnum}\""
  convert -density ${dpi} ${longinpdf} -fill '#80808018' \
    -pointsize 180 -gravity center -draw "rotate -45 text 0,0 '${matnum}'" \
    -quality ${quality} -compress JPEG ${outfolder}/${inpdf}_w.pdf

}  #done # end of PDF loop

if [[ "${cores}" -gt "1" ]]; then
  echo "Parallel execution with ${cores} cores from now on."
  export -f watermark
  parallel -j "${cores}" watermark ::: "${infolder}"/*.pdf ::: "${outfolder}" ::: "${dpi}" ::: "${quality}" ::: "${author}"
else
  for longinpdf in "${infolder}"/*.pdf
  do
    watermark "${longinpdf}" "${outfolder}" "${dpi}" "${quality}" "${author}"
  done # end of PDF loop
fi
