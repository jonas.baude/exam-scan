#!/bin/bash
#
# Author: Steffen Vogel <svogel2@eonerc.rwth-aachen.de>

#{{{ Bash settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
#}}}

# Default values
tplpdf="./exam.pdf"
outfolder="./marked"
size=300
cores=$(nproc)
size=3

#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [--out outfolder] [--pages-expected N] -- sorts and aggreates batch scans based on QR codes. 

Options:
    -h, --help            show this help text
    -t, --template        the template PDF to which the QR codes are added
    -o, --out             output folder: Default: ${outfolder}
    -j, --cores           number of cores for processing. Default: ${cores}
    -s, --size            the size of the QR code. Default: ${size}"

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=ht:o:j:s:
LONGOPTS=help,template:,out:,cores:,size:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "${parsed}"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            echo "$usage"
            exit
            ;;
        -t|--template)
            tplpdf="$2"
            shift 2
            ;;
        -o|--out)
            outfolder="$2"
            shift 2
            ;;
        -s|--size)
            size="$2"
            shift 2
            ;;
        -j|--cores)
            cores="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Check folders
for f in \
        "$outfolder" \
do
    if ! [ -d "$f" ]; then
        echo "Folder $f does not exist. Exiting."
        exit
    fi
done
#}}}

# Loop over all students
doit () {
    matno=$1
    tplpdf=$2
    outfolder=$3
    size=$4

    echo "Generate personalized PDF for ${matno}"

    pages=$(qpdf --show-npages ${tplpdf})

    cp ${tplpdf} ${outfolder}/${matno}.pdf

    qr_code=$(mktemp --suffix=.pdf)

    for pageno in $(seq ${pages}); do
        echo "${matno}-${pageno}" | \
        qrencode -t png -8 -s1 -o - | \
        convert -gravity SouthEast \
                -page A4 png:fd:0 ${qr_code}
        qpdf --replace-input \
             --overlay ${qr_code} \
             --to=${pageno} \
             --from=1 -- \
            ${outfolder}/${matno}.pdf
    done

    rm ${qr_code}
}

matnos=$(cat ${csv} | cut -d ',' -f1)

export SHELL=$(type -p bash)
export -f doit

parallel -j "${cores}" doit ::: "${matnos}" ::: "${tplpdf}" ::: "${outfolder}" ::: ${size}
