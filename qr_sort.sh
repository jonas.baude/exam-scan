#!/bin/bash
#
# Author: Steffen Vogel <svogel2@eonerc.rwth-aachen.de>

#{{{ Bash settings
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
set -o allexport # export all assigned vars
#}}}

# Default values
infolder="./pdfs/ingress"
outfolder="./pdfs/sorted"
errfolder="./pdfs/errors"
dpi=300
cores=$(nproc)
pages_expected=

#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [--out outfolder] [--pages-expected N] -- sorts and aggreates batch scans based on QR codes.

Options:
    -h, --help            show this help text
    -i, --in              input folder with PDFs. Default: ${infolder}
    -o, --out             output folder: Default: ${outfolder}
    -p, --pages-expected  number of expected pages. Used for checking competeness.
    -j, --cores           number of cores for processing. Default: ${cores}
    -d, --dpi             enable upscaling before searching for QR codes."

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=hi:o:e:p:j:
LONGOPTS=help,in:,out:,err:,pages:,dpi:,cores:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            echo "$usage"
            exit
            ;;
        -i|--in)
            infolder="$2"
            shift 2
            ;;
        -o|--out)
            outfolder="$2"
            shift 2
            ;;
        -e|--err)
            errfolder="$2"
            shift 2
            ;;
        -d|--dpi)
            dpi="$2"
            shift 2
            ;;
        -p|--pages-expected)
            pages_expected="$2"
            shift 2
            ;;
        -j|--cores)
            cores="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Check folders
for f in \
        "$infolder" \
        "$outfolder" \
        "$errfolder"
do
    if ! [ -d "$f" ]; then
        echo "Folder $f does not exist. Exiting."
        exit
    fi
done
#}}}

tmpfolder_splitted=$(mktemp -t -d exams-splitted.XXXX)
tmpfolder_scanned=$(mktemp -t -d exams-scanned.XXXX)

echo "Working in temporary folderfor splitted ${tmpfolder_splitted}"
echo "Working in temporary folderfor scanned ${tmpfolder_scanned}"

# Split all PDFs on page boundary
separate() {
  longinpdf=$1
  tmpfolder=$2

  echo "Separating $(basename ${longinpdf})..."

  pfx=$(basename -s .pdf ${longinpdf})

  pdfseparate ${longinpdf} ${tmpfolder}/${pfx}-%d.pdf
}

# Rename all pages according to their barcode
scan_and_move() {
    longinpdf=$1
    tmpfolder=$2

    echo -n "Scanning $(basename ${longinpdf})... "

    if [[ -n ${dpi} ]]; then
        code=$(zbarimg -Sdisable -Sqrcode.enable --raw --quiet <(pdftoppm ${longinpdf} -rx ${dpi} -ry ${dpi}))
    else
        code=$(zbarimg -Sdisable -Sqrcode.enable --raw --quiet ${longinpdf})
    fi

    # Add leading zeroes to the page number.
    code=$(echo $code | awk 'BEGIN { FS = "-" } ; { printf("%s-%s-%s-%03d", $1, $2, $3, $4) }')

    if (( $? == 0 )); then
        echo "-> ${code}.pdf"

        mv ${longinpdf} ${tmpfolder}/${code}.pdf
    else
        echo "-> no QR code detected. Empty page?"

        mkdir -p ${errfolder}
        mv ${longinpdf} ${errfolder}
    fi
}

merge() {
    base=$1
    outfolder=$2

    # sorts by name -> by page
    files=$(ls -1 ${base}-*.pdf | sort)
    dest=${outfolder}/$(basename ${base}).pdf

    pages_scanned=$(echo ${files} | tr ' ' '\n' | wc -l)

    echo "Merging ${pages_scanned} pages: $(basename ${dest})"

    if [[ -n ${pages_expected} ]] && (( ${pages_expected} != ${pages_scanned} )); then
        echo -e "$(tput bold; tput setaf 1)WARNING: Exam $(basename ${dest}) is missing $(( ${pages_expected} - ${pages_scanned})) pages!!!!$(tput sgr0)"
    fi

    pdfunite ${files} ${dest}
}

get_matrnos() {
    tmpfolder=$1

    scans=$(ls -1 ${tmpfolder}/*.pdf | xargs -n1 basename -s ".pdf")
    matrnos=()

    for scan in ${scans}; do
        # QR code content: {base}-{page}
        # e.g. acs-gi4-ss20-312345-42
        #   base: acs-gi4-ss20-312345
        #   pageno: 42
        if [[ ${scan} =~ ^(.+)-([0-9]+)$ ]]; then
            base=${BASH_REMATCH[1]}
            pageno=${BASH_REMATCH[2]}

            matrnos+=("${tmpfolder}/${base}")
        fi
    done

    echo "${matrnos[@]}"
}

export SHELL=$(type -p bash)
export -f separate merge scan_and_move

echo "Separating all scanned PDFs into separate documents"
parallel -j ${cores} separate ::: ${infolder}/*.pdf ::: ${tmpfolder_splitted}

echo "Decode barcodes..."
parallel -j ${cores} scan_and_move ::: ${tmpfolder_splitted}/*.pdf ::: ${tmpfolder_scanned}

# Find all Matrno's
matrnos=$(get_matrnos ${tmpfolder_scanned} | tr ' ' '\n' | sort -u)

echo "Merge all pages per matrno"
parallel -j ${cores} merge ::: ${matrnos} ::: ${outfolder}
