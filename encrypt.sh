#!/bin/bash
#
# Author: Christian Rohlfing <rohlfing@ient.rwth-aachen.de>, Dietmar Kahlen <dk@ithe.rwth-aachen.de>

#{{{ Bash settings
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
#}}}

# Default values
tmpfolder=$(mktemp -d)
v=n
password=-
infolder="./pdfs/watermarked"
outfolder="./pdfs/encrypted"
globalpassword=y


#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [-out outfolder] [-p n] --  encrypt exam scans in folder 'in' and puts them in folder 'out'. 
Password for each student generated with 'pwgen' or given one is used for all students.  Password(s) are stored in 'out'/passwords.csv
Attention: contents of folder 'out' will be overwritten in the following!

Options:
    -h, --help      show this help text
    -i, --in        input folder with PDFs. Default: ${infolder}
    -o, --out       output folder. Default: ${outfolder}
    -p, --password  sets global password. Default empty, such that each PDF gets a custom password generated with 'pwgen'"

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=hi:o:p:v
LONGOPTS=help,in:,out:,password:,verbose

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            echo "$usage"
            exit
            ;;
        -i|--in)
            infolder="$2"
            shift 2
            ;;
        -o|--out)
            outfolder="$2"
            shift 2
            ;;
        -p|--password)
            password="$2"
            shift 2
            ;;
        -v|--verbose)
            v=y
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Check folders
for f in \
        "$infolder" \
        "$outfolder"
do
    if ! [ -d "$f" ]; then
        echo "Folder $f does not exist. Exiting."
        exit
    fi
done
#}}}

if [[ $password = "-" ]]; then
  globalpassword=n
fi

#{{{ Variables
readonly passwordfile="${outfolder}/passwords.csv"
#}}}

# Remove old password file
rm -f "$passwordfile"

# List all PDFs
echo
echo "Available PDFs"
echo
ls -a1 "${infolder}"/*.pdf
echo

echo 
echo "Start encryption"

# Loop over all PDFs
for longinpdf in "${infolder}"/*.pdf
do
  # Get matriculation number from file
  inpdf=$(basename "${longinpdf%.*}")  # file name without folder and extension
  matnum=${inpdf:0:6}  # read in first 6 letters

  case $matnum in
    ''|*[!0-9]*) echo "Warning: $matnum not a matriculation number. I'm ignoring it for now." && continue ;;
  esac

  echo "Handle ${longinpdf}"
  
  
  # Encryption
  longoutpdf="${outfolder}"/"${inpdf}"_aes.pdf
  
  if [[ $globalpassword = "y" ]]; then
    pw="${password}"  # global password
  else
    pw=$(pwgen 8 1)  # unique password
  fi
  
  # Encrypt PDF
  qpdf --encrypt ${pw} ${pw} 256 -- "${longinpdf}" "${longoutpdf}"

  # Store password per matriculation number
  echo "${matnum}, ${pw}" >> "${passwordfile}"

done # end of PDF loop

echo
echo "Done. Passwords stored in ${passwordfile}."
echo

exit
